
citbx_use "dockerimg"

job_setup() {
    # Only on workstation, set the suitable tag prefix if not set
    CI_COMMIT_TAG="$CI_JOB_NAME/${CI_COMMIT_TAG#$CI_JOB_NAME/}"
}

job_main() {
    local pattern='^'"$CI_JOB_NAME"'/.*$'
    if ! [[ $CI_COMMIT_TAG =~ $pattern ]]; then
        print_critical "This job cannot be launched with the following tag '$CI_COMMIT_TAG'" \
                        "This error is probably due to:" \
                        " * on a Gitlab runner: Missing or incorrect 'only' tag in the .gitlab-ci.yml - You can put this one:" \
                        "    only:" \
                        "        - /^$CI_JOB_NAME\/.*\$/" \
                        " * on your local worspace: You have launched ci-toolbox $CI_JOB_NAME with the wrong tag - Try with the additional option:" \
                        "    --image-tag $CI_JOB_NAME/x.y.z"
    fi
    docker build -f bootp-registry/Dockerfile -t $CI_REGISTRY_IMAGE/bootp-registry:${CI_COMMIT_TAG#bootp-registry/} .
    if [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN ${CI_REGISTRY};
        docker push $CI_REGISTRY_IMAGE/bootp-registry:${CI_COMMIT_TAG#bootp-registry/};
    fi
}

job_after() {
    local retcode=$1
    if [ $retcode -eq 0 ]; then
        local imgname="$CI_REGISTRY_IMAGE/bootp-registry:${CI_COMMIT_TAG#bootp-registry/}"
        print_info "Image \"$imgname\" successfully generated" \
                    "> You can now test this image by running the following script like this:" \
                    "> $ $CI_PROJECT_DIR/bootp-registry/start-registry-devtool.sh $imgname"
    fi
}
