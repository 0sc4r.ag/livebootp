
citbx_use "dockerimg"

job_setup() {
    citbx_run_ext_job "${CI_JOB_NAME%-mksquashfs}-build"
    # Only on workstation, set the suitable tag prefix if not set
    local job_prefix=${CI_JOB_NAME%-mksquashfs}
    CI_COMMIT_TAG="$job_prefix/${CI_COMMIT_TAG#$job_prefix/}"
}

job_main() {
    local pattern='^target-.*-mksquashfs$'
    if ! [[ $CI_JOB_NAME =~ $pattern ]]; then
        print_critical "Target image job name must begin by 'target-' and terminate by '-mksquashfs'"
    fi
    LIBEBOOTP_TARGET_NAME=${CI_JOB_NAME%-mksquashfs}
    pattern='^'"$LIBEBOOTP_TARGET_NAME"'/.*$'
    if ! [[ $CI_COMMIT_TAG =~ $pattern ]]; then
        print_critical "This job cannot be launched with the following tag '$CI_COMMIT_TAG'" \
                        "This error is probably due to:" \
                        " * on a Gitlab runner: Missing or incorrect 'only' tag in the .gitlab-ci.yml - You can put this one:" \
                        "    only:" \
                        "        - /^$LIBEBOOTP_TARGET_NAME\/.*\$/" \
                        " * on your local worspace: You have launched ci-toolbox $CI_JOB_NAME with the wrong tag - Try with the additional option:" \
                        "    --image-tag $LIBEBOOTP_TARGET_NAME/x.y.z"
    fi
    LIBEBOOTP_TARGET_NAME=${LIBEBOOTP_TARGET_NAME#target-}
    LIBEBOOTP_TARGET_VERSION=${CI_COMMIT_TAG#${CI_JOB_NAME%-mksquashfs}/}
    LIBEBOOTP_DOCKER_IMAGE="$CI_REGISTRY_IMAGE/$LIBEBOOTP_TARGET_NAME:$LIBEBOOTP_TARGET_VERSION"
    LIBEBOOTP_IMAGE_DIR="${LIBEBOOTP_TARGET_NAME}/$LIBEBOOTP_TARGET_VERSION"
    LIBEBOOTP_IMAGE_PATH="$CI_PROJECT_DIR/artifacts/$LIBEBOOTP_IMAGE_DIR"
    # Export docker filesystem
    LIBEBOOTP_DOCKER_EXPORT_ID=$(docker run -d "$LIBEBOOTP_DOCKER_IMAGE" sleep infinity)
    print_note "Extracting rootfs from $LIBEBOOTP_DOCKER_IMAGE docker image..."
    mkdir -p rootfs
    docker export $LIBEBOOTP_DOCKER_EXPORT_ID | sudo tar -C rootfs -x
    # Convert VOLUME instruction to add_pertistant_storage commands
    if [ $(docker image inspect "$LIBEBOOTP_DOCKER_IMAGE" \
        | jq -r '.[0].ContainerConfig.Volumes | length') -gt 0 ]; then
        while read -r line; do
            echo "add_pertistant_storage \"$line\" bind copy"
        done <<< "$(docker image inspect "$LIBEBOOTP_DOCKER_IMAGE" \
            | jq -r '.[0].ContainerConfig.Volumes | keys[]')" \
            | sudo sudo tee -a rootfs/etc/livebootp/startup.d/40_pesistant-volumes.sh > /dev/null
    fi
    # Extract vmlinuz & initrd.img files
    mkdir -p $LIBEBOOTP_IMAGE_PATH
    local vmlinuz_file=$(find rootfs/boot/ -type f -name "vmlinuz-*")
    if [ -z "$vmlinuz_file" ]; then
        print_critical "You must have one kernel image installed (e.g.: linux-image-generic) into the docker image"
    fi
    if [ "$(wc -l <<< "$vmlinuz_file")" -ne 1 ]; then
        print_critical "You cannot have more than one kernel image installed into the docker image"
    fi
    local kernel_version=${vmlinuz_file:20}
    sudo mv rootfs/boot/vmlinuz-$kernel_version $LIBEBOOTP_IMAGE_PATH/vmlinuz
    sudo mv rootfs/boot/initrd.img-$kernel_version $LIBEBOOTP_IMAGE_PATH/initrd.img
    if [ -f "rootfs/boot/livebootp-cmdline" ]; then
        sudo mv rootfs/boot/livebootp-cmdline $LIBEBOOTP_IMAGE_PATH/cmdline
    fi
    # Write image info
    printf '%s="%s"'"\n" LIBEBOOTP_TARGET_NAME $LIBEBOOTP_TARGET_NAME \
        LIBEBOOTP_TARGET_VERSION $LIBEBOOTP_TARGET_VERSION \
        LIBEBOOTP_DOCKER_IMAGE $LIBEBOOTP_DOCKER_IMAGE \
        | sudo sudo tee -a rootfs/etc/os-release > /dev/null
    # Generate squashfs file
    sudo mksquashfs rootfs $LIBEBOOTP_IMAGE_PATH/rootfs.squashfs -no-recovery -noappend -comp lzo -e boot
}

job_after() {
    local retcode=$1
    docker rm -f $LIBEBOOTP_DOCKER_EXPORT_ID || true
    sudo rm -rf rootfs
    if [ -d $LIBEBOOTP_IMAGE_PATH ]; then
        sudo chmod +rX -R $LIBEBOOTP_IMAGE_PATH
        sudo chown $(id -u):$(id -g) -R $LIBEBOOTP_IMAGE_PATH
    fi
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$LIBEBOOTP_IMAGE_DIR\" successfully generated"
    fi
}
